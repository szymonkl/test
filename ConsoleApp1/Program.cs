﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Adam adam = new Adam();
            Test test = new Test();
            test.TestProperty = "testvalue";
            Console.WriteLine(new Validator().Validate(adam.Name));
        }
    }

    public class Adam
    {
       public static void RobObiad() { }
       public string Name { get { return "Adam"; } }
    }
}
